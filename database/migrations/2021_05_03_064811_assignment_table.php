<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('student_id');
            $table->foreign('student_id')->references('id')->on('student');

            $table->bigInteger('level_tutored_id');
            $table->foreign('level_tutored_id')->references('level_id')->on('common_level');
            
            $table->bigInteger('tutor_race_id');
            $table->foreign('tutor_race_id')->references('race_id')->on('common_race');
            
            $table->bigInteger('tutor_qualification_id');
            $table->foreign('tutor_qualification_id')->references('qualification_id')->on('common_qualification');
            
            $table->unsignedBigInteger('tutor_id');
            $table->foreign('tutor_id')->references('id')->on('users');
            
            $table->unsignedBigInteger('closed_by_id');
            $table->foreign('closed_by_id')->references('id')->on('users');



            $table->string('assignment_status',20);
            $table->string('tutor_gender',15);
            $table->string('additional_information',15);
            $table->string('lessions_per_month',15);
            $table->string('hours_per_lession',15);
            $table->string('rates',15);
            $table->string('time_slot',15);
            $table->string('tution_commence_period',15);
            $table->string('period_from',15);
            $table->string('period_to',15);
            $table->string('online_platform_status',15);
            $table->string('knowldege_about_us_from',15);
            $table->string('created_by',15);
            $table->timestamps();


       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment');
    }
}
