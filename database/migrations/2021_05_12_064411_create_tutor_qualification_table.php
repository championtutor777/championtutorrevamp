<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorQualificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_qualification', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tutor_id');
            $table->foreign('tutor_id')->references('id')->on('users');
            $table->unsignedBigInteger('qualification_id');
            $table->foreign('qualification_id')->references('id')->on('qualification');
            $table->text('type');
            $table->text('major_course');
            $table->string('educational_result');
            $table->text('period_from');
            $table->text('period_to');
            $table->integer('current');
            $table->text('grade');
            $table->text('type_music');
            $table->text('music_achievements');
            $table->text('type_of_languages');
            $table->text('language_achievements');
            $table->text('other_talent');
            $table->text('other_achievements');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_qualification');
    }
}
