<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TutorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('image');
            $table->string('nric',40)->nullable();
            $table->bigInteger('race_id');
            $table->foreign('race_id')->references('race_id')->on('common_race')->nullable();

            $table->bigInteger('religion_id');
            $table->foreign('religion_id')->references('religion_id')->on('common_religion')->nullable();

            $table->integer('country_id');
            $table->foreign('country_id')->references('id')->on('countries');

            $table->bigInteger('education_level_id');
            $table->foreign('education_level_id')->references('educational_id')->on('common_educational_level');


            $table->bigInteger('certified_teacher_id');
            $table->foreign('certified_teacher_id')->references('credentials_id')->on('common_teaching_credentials');

            $table->string('occupation',50);
            $table->string('postal_code',10);
            $table->bigInteger('home_phone');

            $table->enum('tutoring_status', ['Full Time', 'Part Time']);
            $table->string('timezone',100);
            $table->string('email_notification',5);
            $table->string('tutor_status',15);
            $table->text('experience');
            $table->string('show_profile_public',5);
            $table->text('how_do_you_get_know');
            $table->string('interest_to_teach_online',5);
            $table->text('not_sure_reason');
            $table->string('interest_to_teach_tuition_centre',5);
            $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor');
    }
}
