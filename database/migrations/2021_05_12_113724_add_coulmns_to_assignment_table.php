<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoulmnsToAssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assignment', function (Blueprint $table) {
            
            $table->unsignedBigInteger('assigned_to');
            $table->foreign('assigned_to')->references('id')->on('users');
            $table->enum('message_mode',['WhatsApp','SMS']);
            $table->string('discount_code',20);
            $table->text('pending_comments');
            $table->date('date_of_closure');
            $table->date('start_date');
            $table->string('commission',15);
            $table->string('billable_amount',15);
            $table->date('payment_date');
            $table->string('start_time');
            $table->integer('decided_to_stop');
            $table->integer('non_credit_card_payment');
            $table->integer('remain_in_PL');
            $table->text('payment_remarks');
            $table->integer('follow_up');
            $table->integer('unsubscribe');
            $table->integer('do_not_send_feedback');
            $table->integer('show_in_google_map');
            $table->text('lost_reason');
            $table->text('lost_remarks');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assignment', function (Blueprint $table) {
            //
        });
    }
}
