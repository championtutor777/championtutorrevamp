<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            
            $table->id();
            $table->integer('country_id');
            $table->foreign('country_id')->references('id')->on('countries');

            $table->bigInteger('race_id');
            $table->foreign('race_id')->references('race_id')->on('common_race');

            $table->unsignedBigInteger('created_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->string('name',80);
            $table->string('identity',60);
            $table->string('child_name',80);
            $table->string('email',60);
            $table->string('gender',10);
            $table->date('dob');
            $table->string('residential_type',100);
            $table->string('block',100);
            $table->string('street',100);
            $table->string('unit',100);
            $table->string('postal_code',60);
            $table->string('phone_number',50);
            $table->string('school',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
